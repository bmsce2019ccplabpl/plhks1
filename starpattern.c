#include<stdio.h>
int input()
{
   int n;
   printf("Enter the number of rows\n");
   scanf("%d",&n);
   return n;
}

void display(int a)
{
   for(int i=1; i<=a; i++)
   {
      for(int j=1;j<=i;j++)
      {
         printf("*");
      }
      printf("\n");
   }
}

int main()
{
   int a=input();
   display(a);
   return 0;
}